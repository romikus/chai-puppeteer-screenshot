pixelmatch = require 'pixelmatch'
PNG = require('pngjs').PNG
fs = require 'fs'
path = require 'path'
{sep} = path

class ScreenshotMatcher
  constructor: (@page, @name) ->
    @viewports = {}
    @filepathes = {}
    @dirpath = __dirname + sep + config.dirname

  setViewport: (viewport) ->
    if typeof viewport is 'string'
      value = config.viewports[viewport]
      if typeof value is 'string'
        @setViewport value
      else if Array.isArray value
        @setViewport value
      else
        @viewports[viewport] = value
    else if Array.isArray viewport
      for item in viewport
        @setViewport item
    @setFilepathes()

  setParams: (@params) ->

  setFilepathes: ->
    for viewport of @viewports
      @filepathes[viewport] = @dirpath + sep + @name + '-' + viewport + '.png'

  start: ->
    new Promise @startFlow.bind @

  startFlow: (@resolve, @reject) ->
    fs.mkdir @dirpath, @mkdirCallback.bind @

  mkdirCallback: (err) ->
    if err and err.code isnt 'EEXIST'
      return @reject err

    @wait = 0
    @result = {}
    for viewport, filepath of @filepathes
      @wait++
      child = Object.create @
      child.viewport = viewport
      child.filepath = filepath
      child.parent = @
      fs.access filepath, fs.constants.F_OK, @accessCallback.bind child

  accessCallback: (notExists) ->
    if notExists
      @takeAndSaveScreenshot()
    else
      @takeAndCompareScreenshot()

  takeAndCompareScreenshot: ->
    @takeScreenshot @readAndCompareScreenshots.bind(@), @throw.bind(@)

  readAndCompareScreenshots: (imageData) ->
    @filesRead = 0
    @compareScreenshots = @compareScreenshots.bind @
    @setCurrentImage imageData
    @setPreviousImage()

  setCurrentImage: (imageData, callback) ->
    new PNG().parse imageData, @setCurrentImageCallback.bind @

  setPreviousImage: ->
    @previousImage = fs.createReadStream(@filepath).pipe(new PNG()).on('error', @throw).on 'parsed', @compareScreenshots

  compareScreenshots: ->
    if ++@filesRead < 2
      return

    {width, height} = @previousImage
    @diff = new PNG {width, height}
    differentPixels = pixelmatch @previousImage.data, @currentImage.data, @diff.data, width, height,
      config.pixelmatch

    if differentPixels
      @saveCurrentAndDiff()
    else
      @success()

  saveCurrentAndDiff: ->
    @filesWrite = 0
    @saveCurrentAndDiffCallback = @saveCurrentAndDiffCallback.bind @
    @saveCurrent()
    @saveDiff()

  saveCurrent: ->
    @currentPath = @dirpath + sep + @name + '-' + @viewport + '-current.png'
    @currentImage.pack().pipe(fs.createWriteStream @currentPath).on 'finish', @saveCurrentAndDiffCallback
  
  saveDiff: ->
    diffPath = @dirpath + sep + @name + '-' + @viewport + '-diff.png'
    @diff.pack().pipe(fs.createWriteStream diffPath).on 'finish', @saveCurrentAndDiffCallback

  saveCurrentAndDiffCallback: ->
    if ++@filesWrite < 2
      return
  
    @finish @currentPath

  setCurrentImageCallback: (err, data) ->
    if err
      return @throw err
    
    @currentImage = data
    @compareScreenshots()

  takeAndSaveScreenshot: ->
    @takeScreenshot @success.bind(@), @throw.bind(@), @filepath

  takeScreenshot: (success, error, filepath) ->
    saveScreenshot @page, @viewports[@viewport], @params, filepath, success, error

  success: ->
    @finish @filepath

  finish: (filepath) ->
    if @rejected
      return

    @result[@viewport] = filepath
    @parent.wait--
    unless @parent.wait
      @resolve @result

  throw: (err) ->
    @parent.rejected = true
    @reject err

saveScreenshot = (page, viewport, params, filepath, success, error) ->
    item = {viewport, params, filepath, success, error}
    if page.screenshotQueue
      page.screenshotQueue.push item
    else
      page.screenshotQueue = [item]
      nextScreenshot page

nextScreenshot = (page) ->
  item = page.screenshotQueue[0]
  await page.setViewport item.viewport
  item.params.path = item.filepath
  try
    result = await page.screenshot item.params
    item.success result
  catch err
    item.error err

  if page.screenshotQueue.length is 1
    delete page.screenshotQueue
  else
    page.screenshotQueue.shift()
    nextScreenshot page

module.exports = config = (chai, utils) ->
  chai.use require "chai-as-promised"
  {assert, Assertion} = chai
  utils.addMethod Assertion.prototype, 'matchScreenshot', (name, viewportName, params) ->
    screenshotMatcher = new ScreenshotMatcher @_obj, name
    if typeof viewportName is 'string' or Array.isArray viewportName
      screenshotMatcher.setViewport viewportName
      screenshotMatcher.setParams params
    else
      screenshotMatcher.setViewport 'default'
      screenshotMatcher.setParams viewportName

    assert.eventually.deepEqual screenshotMatcher.start(), screenshotMatcher.filepathes, "screenshots are not equal"

config.dirname = 'screenshots'
config.pixelmatch = threshold: 0.1
